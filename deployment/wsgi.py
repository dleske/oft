# add application to Python path.  If done in wsgi call, will be prepended and
# mess up import of system ldap library over application's ldap module
import sys
sys.path.append('src/')

# create app object invoked by wsgi
from src import create_app
app = create_app()
