CHANGELOG	:= CHANGELOG.md
VERSION		:= $(shell git describe --tags --dirty | sed -e 's/^v//' -e 's/-/+/' -e 's/-/./g')
APP		:= dummy
IMAGE		:= dleske/dummy
TAG		:= $(shell git describe --tags --dirty | sed -e 's/^v//')

.PHONY: changelog
changelog:
	@printf "# Changelog\n\n" > $(CHANGELOG)
	@format='## %(tag) (%(*committerdate:format:%Y-%m-%d)) %(subject)%0a%0a%(body)' ; \
        tags=`git for-each-ref refs/tags \
                 --format='%(objecttype) %(refname:lstrip=2)' \
                | awk '$$1 == "tag" { print $2 }'`; \
        git tag --list --sort=-\*committerdate \
                --format="$$format" $$tags \
	>> $(CHANGELOG)

.PHONY: clean-tests
clean-tests:
	rm -f .pytest_cache/v/cache/stepwise
	rm -f .lintcache

.PHONY: test
test:
	tests/test-all --all --stepwise

.PHONY: test-clean
test-clean:
	tests/test-all --all

# fails if version doesn't match specific pattern, so as not to publish
# development version.
.PHONY: checkversion
checkversion:
	@echo "Checking that $(VERSION) is a release version"
	@[[ $(VERSION) =~ ^([0-9]+\.)*[0-9]+$$ ]]
	@echo "Checking that $(VERSION) is in setup.cfg"
	@grep -qw "$(VERSION)" setup.cfg

$(PACKAGES): $(SOURCES)
	@echo Version: $(VERSION)
	@python3 -m build

docs/%.md: src/%.py misc/pdoc-templates/text.mako
	@pdoc --template-dir=misc/pdoc-templates $(APP).$* > $@

docs: docs/doc1.md docs/doc2.md

.PHONY: image
image: $(PACKAGES) deployment/Dockerfile.uwsgi
	#@test -n "$(REGISTRY)" || ( echo "Please specify REGISTRY for an image repository" && false )
	#docker build -t $(REGISTRY)/$(IMAGE):$(TAG) -f deployment/Dockerfile.uwsgi .
	docker build -t $(IMAGE):$(TAG)-amd64 --platform amd64 -f deployment/Dockerfile.uwsgi .
	docker build -t $(IMAGE):$(TAG)-arm64 --platform arm64 -f deployment/Dockerfile.uwsgi .

# We don't need to check that REGISTRY is defined because we rebuild `image`
# target every time anyway.
.PHONY: image-publish
image-publish: image
	#docker push $(REGISTRY)/$(IMAGE):$(TAG)
	docker push $(IMAGE):$(TAG)-amd64
	docker push $(IMAGE):$(TAG)-arm64
	docker manifest create $(IMAGE):$(TAG) \
		--amend $(IMAGE):$(TAG)-amd64 \
		--amend $(IMAGE):$(TAG)-arm64
	docker manifest push $(IMAGE):$(TAG)

.PHONY: image-publish-latest
image-publish-latest: image-publish
	docker manifest create $(IMAGE):latest \
		--amend $(IMAGE):$(TAG)-amd64 \
		--amend $(IMAGE):$(TAG)-arm64
	docker manifest push $(IMAGE):latest

# Commenting out the following so I hopefully never publish an empty "dummy"
# package to PyPi
#publish-test: $(PACKAGES) checkversion
#	@python3 -m twine upload --repository testpypi $(PACKAGES)
#
#publish: $(PACKAGES) checkversion
#	@python3 -m twine upload --repository pypi $(PACKAGES)
