# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=wildcard-import,unused-wildcard-import
#
from src.ajax import _must_have

def tests__must_have():
  assert _must_have({}, ['hi']) == ['hi']
  assert _must_have({}, ['hi', 'bye']) == ['hi', 'bye']
  assert _must_have({'bye':1}, ['hi']) == ['hi']
  assert _must_have({'bye':1}, ['hi', 'bye']) == ['hi']
  assert _must_have({'bye':1}, ['bye']) is None
  assert _must_have({'hi':1, 'bye':1}, ['hi', 'bye']) is None
