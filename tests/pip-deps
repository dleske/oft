#!/usr/bin/env python3

import sys
import pkg_resources
from pkg_resources import DistributionNotFound, VersionConflict

# check arguments
if len(sys.argv) < 2:
  sys.stderr.write("Please specify one or more requirements files.\n")
  sys.exit(128)

# read dependencies from files
missing = []
wrong_version = []
for file in sys.argv[1:]:
  with open(file, 'r') as fp:
    for line in fp:
      dependency = line.rstrip()
      try:
        pkg_resources.require(dependency)
      except DistributionNotFound:
        missing.append(dependency)
      except VersionConflict:
        wrong_version.append(dependency)

# flag issues if necessary
if missing or wrong_version:
  sys.stderr.write("Dependency check failed.\n")
  if missing:
    sys.stderr.write(f"Missing packages: {', '.join(missing)}.\n")
  if wrong_version:
    sys.stderr.write(f"Version mismatch: {', '.join(wrong_version)}.\n")
  sys.exit(2)

print("Python packages installed as specified.")
