#!/bin/bash
# 
# Run all available test suites from a single script.

# --------------------------------------------------------------------------
#                                                            configuration
# --------------------------------------------------------------------------

#PYTHONPATH=.

# --------------------------------------------------------------------------
#                                                            functions
# --------------------------------------------------------------------------

fail_to()
{
  2>&1 echo Unable to $@
  exit 1
}

warn_couldnt()
{
  2>&1 echo Unable to $@
}

# --------------------------------------------------------------------------
#                                                         argument parsing
# --------------------------------------------------------------------------

usage()
{
  cat <<EOF
Usage: $(basename $0) [options] [case ...]
Options:
  --no-sqlite:      Don't test SQLite (default to test SQLite)
  --pgsql:          Test Postgres
  --selenium:       Execute Selenium testing (default false)
  -a|--all:         Test all above if containers available
  --no-linting:     Skip lint testing
  -x|--exitfirst:   [pytest] Exit at first failed test
  -v|--verbose:     [pytest] Show verbose information
  -d|--debug:       [pytest] Show debug-level logging
  --sw|--stepwise:  [pytest] Exit and resume at first failed test

Postgres and Selenium testing can only be performed if an appropriate
container is running with the label 'postgres-myapp' or 'selenium-myapp'.

Specific Pytest test cases may be specified following options.

EOF
}

# defaults
sqlite=1
pgsql=0
selenium=0
linting=1
exitfirst=0
verbose=0
debug=0
sqlite=1
stepwise=0

# get arguments
while [ -n "$1" ]
do
  case "$1" in
    -a|--all)
      sqlite=1
      pgsql=1
      selenium=1
      ;;
    --no-sqlite)
      sqlite=0
      ;;
    --pgsql)
      pgsql=1
      ;;
    --no-linting)
      linting=0
      ;;
    --selenium)
      selenium=1
      ;;
    -d|--debug)
      debug=1
      ;;
    -v|--verbose)
      verbose=1
      ;;
    -x|--exitfirst)
      exitfirst=1
      ;;
    --sw|--stepwise)
      stepwise=1
      ;;
    -h|--help)
      usage
      exit
      ;;
    -*|--*)
      echo "Unrecognized option: $1"
      usage
      exit 1
      ;;
    *)
      tests="$tests $1"
      ;;
  esac
  shift
done

pytest_opts="--cov=src --cov-report=term --cov-report=html"
if (( debug ))
then
  pytest_opts="$pytest_opts --log-level=debug"
fi
if (( verbose ))
then
  pytest_opts="$pytest_opts -v"
fi
if (( exitfirst ))
then
  pytest_opts="$pytest_opts -x"
fi
if (( stepwise ))
then
  pytest_opts="$pytest_opts --sw"
fi

# --------------------------------------------------------------------------
#                                                     actually doing stuff
# --------------------------------------------------------------------------

export PYTHONPATH=.:$PYTHONPATH

# linting
if (( linting ))
then
  tests/linting/intest yamllint pylint || exit 1
fi

# test sqlite if desired
sqlite_testing=""
if (( sqlite ))
then
  sqlite_testing="--db=sqlite"
fi

# test pgsql if desired and container is running
pgsql_testing=""
pgctr=0
docker ps | grep -q postgres-myapp && pgctr=1
if (( pgsql ))
then
  if (( pgctr ))
  then
    pgsql_testing="--db=pgsql"
  else
    warn_couldnt test Postgres
  fi
fi

# test Selenium if desired and container is running
selenium_testing=""
if (( selenium ))
then
  if docker ps | grep -q selenium-myapp
  then
    env="$env MYAPP_STATIC_RESOURCE_URI=http://resources:8080 SELENIUM_URL=0.0.0.0:4444/wd/hub"
    selenium_testing="tests/test_selenium.py"
  else
    warn_couldnt test Selenium
  fi
fi

# functional testing
if [ -z "$tests" ]
then
  tests="$sqlite_testing $pgsql_testing $selenium_testing"
fi
sh -c "$env pytest $pytest_opts $tests tests/test.py || exit 1"
