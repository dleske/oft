# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import pytest


# allow selection of database types with '--db=sqlite --db=pgsql'
def pytest_addoption(parser):
  parser.addoption('--db', action="append", default=[], help='sqlite, pgsql, or both')


# the basic web client
@pytest.fixture(scope='class')
def client(app):
  return app.test_client()


# for testing CLI operations
@pytest.fixture
def runner(app):
  return app.test_cli_runner()
