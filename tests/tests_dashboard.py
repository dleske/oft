# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#


def test_dashboard_login_requires_authn(client):
  """
  Test that login access is denied if user is unauthenticated.
  """
  response = client.get('/login')
  assert response.status_code == 401


def test_dashboard_without_authn(client):
  """
  Test that dashboard access is available for unauthenticated users and that
  they are presented with the option to log in.
  """
  response = client.get('/')
  assert "Login" in response.data.decode('utf-8')
  assert response.status_code == 200


def test_dashboard_with_authn(client):
  """
  Test that dashboard access is available for authenticated users and that
  they are presented with a simple greeting.
  """
  response = client.get('/login', follow_redirects=True,
    environ_base={'HTTP_X_FORWARDED_USER': 'user@example.org'}
  )
  assert response.status_code == 200
  assert "Hello, user@example.org" in response.data.decode('utf-8')
