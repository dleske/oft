# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=wildcard-import,unused-wildcard-import
#
# This file simply imports all tests from the various files.  Import * from
# here in pytest scripts.
#
from tests_ajax import *
from tests_status import *
from tests_admin import *
from tests_dashboard import *
from tests_cli import *
