# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#


def test_admin_denied_unauthenticated(client):
  """
  Test that admin access is denied if user is unauthenticated.
  """
  response = client.get('/admin/')
  assert response.status_code == 401


def test_admin_denied_unauthorized(client):
  """
  Test that admin access is denied if user is unauthorized.
  """
  response = client.get('/admin/', environ_base={'HTTP_X_FORWARDED_USER': 'blurg'})
  print(f"Received status code {response.status_code}")
  assert response.status_code == 403


def test_admin_denied_unauthorized_2(client):
  """
  Test that admin access is denied if regular user does not have admin access.
  """
  response = client.get('/admin/', environ_base={'HTTP_X_FORWARDED_USER': 'user@example.org'})
  print(f"Received status code {response.status_code}")
  assert response.status_code == 403


def test_admin_direct(client):
  """
  Test that admin access is granted if user is authorized.
  """
  response = client.get('/admin/', environ_base={'HTTP_X_FORWARDED_USER': 'admin@example.org'})
  print(f"Received status code {response.status_code}")
  assert response.status_code == 200
