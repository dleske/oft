# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=redefined-outer-name,unused-wildcard-import,wildcard-import
#
import os
import tempfile
import pytest
from tests_all import *
import medial
from src import create_app
from src.db import init_db, seed_db
from src.notifier import clear_notifiers


# common test params
test_params = [
  {
    'name': 'sqlite',
    'schema': 'schema.sql',
    'seed': '../tests/data.sql',
    'delete_afterwards': True,
  }, {
    'name': 'pgsql',
    'schema': 'schema.psql',
    'seed': '../tests/data.sql',
    'uri': os.environ.get('MYAPP_PGSQL_URI', 'postgresql://postgres:supersecretpassword@localhost:5432/postgres')
  }
]
test_ids = [ x['name'] for x in test_params ]


@pytest.fixture(scope='class', params=test_params, ids=test_ids)
def app(request):

  shouldskip = request.param['name'] not in request.config.getoption('--db')
  if shouldskip:
    pytest.skip(f"{request.param['name']} tests not selected")

  print(f"Request ID: {request.__dict__}")
  if 'uri' in request.param:
    uri = request.param['uri']
  else:
    (filehandle, filename) = tempfile.mkstemp()
    uri = 'file://' + filename

  app = create_app({
    'TESTING': True,
    'DATABASE_URI': uri,
    'CONFIG': 'tests/app.conf'
  })

  with app.app_context():
    medial.configure(uri)
    init_db()

  yield app

  if request.param.get('delete_afterwards', False):
    os.close(filehandle)
    os.unlink(filename)


@pytest.fixture(scope='class', params=test_params, ids=test_ids)
def seeded_app(request):

  print(f"Request ID: {request.id}")
  if 'uri' in request.param:
    uri = request.param['uri']
  else:
    (filehandle, filename) = tempfile.mkstemp()
    uri = 'file://' + filename

  app = create_app({
    'TESTING': True,
    'DATABASE_URI': uri,
    'CONFIG': 'tests/app.conf'
  })

  with app.app_context():
    medial.configure(uri)
    init_db()
    seed_db(request.param['seed'])
    clear_notifiers()

  yield app

  if request.param.get('delete_afterwards', False):
    os.close(filehandle)
    os.unlink(filename)
