# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
from src.db import init_db_command, seed_db_command


# ---------------------------------------------------------------------------
#                                                              CLI commands
# ---------------------------------------------------------------------------

def test_init_db(app):
  runner = app.test_cli_runner()
  result = runner.invoke(init_db_command)
  assert "Initialized the database." in result.output


def test_seed_db(app):
  runner = app.test_cli_runner()
  result = runner.invoke(seed_db_command, 'tests/data.sql')
  print(result.output)
  assert "Initialized and seeded the database." in result.output
