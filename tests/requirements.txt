# used for inspection testing (linting)
yamllint
pylint==2.13.5
# used for functional testing
pytest==7.1.1
pytest-cov==3.0.0
# used for automated client testing
#pytest-selenium==2.0.1
#selenium-wire==4.3.0
