# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
from src.db import SCHEMA_VERSION


def test_status(client):
  """
  Test that /status returns a 200 in the case of the app responding.
  """
  response = client.get('/status/')
  print(response.data)
  assert response.data == "I'm: Okay".encode('utf-8')
  assert response.status_code == 200


def test_status_services_db(client):
  """
  Test that /status/services/db returns a 200 in the case of things working.
  """
  response = client.get('/status/services/db')
  print(response.data)
  assert response.data == f'DB: Okay (schema version {SCHEMA_VERSION})'.encode('utf-8')
  assert response.status_code == 200


def test_status_services(client):
  """
  Test that /status/services returns a 200 in the case of things working.
  """
  response = client.get('/status/services')
  print(response.data)
  assert response.data == f'DB: Okay (schema version {SCHEMA_VERSION})'.encode('utf-8')
  assert response.status_code == 200
