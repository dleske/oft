# Changelog

## v0.3.0 (2022-07-22) Publish "dummy" application image

Added:
- Publish application as "dummy" image
- authz_by_groups now gets memberships from configuration
- Updated routing where "/" is not authenticated but "/login" is, for
  applications where unauthenticated but reduced access is permitted

Updated:
- Use pdoc instead of pdoc3

Fixed:
- Use of SQLite database no longer tests instance directory unnecessarily
- Entitlement logic now grants all entitlements, not just the first possible

## v0.2.1 (2022-06-21) Add Medial to requirements

## v0.2 (2022-06-21) Use Medial

Use Medial package from PyPi instead of `mdal` submodule.

## v0.1 (2022-05-05) Initial implementation complete


