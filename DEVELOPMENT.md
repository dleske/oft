# Development and testing

## Getting started

### Create a virtual environment

    $ python3 -m venv venv
    $ . venv/bin/activate
    $ pip install -r requirements.txt   # for getting application requirements
    $ pip install -r tests/requirements.txt   # ...testing requirements

### Retrieving submodules

There are two submodules in this repository:

- the `mdal` module, which provides database access used by the application
- the `intest` module, which handles inspection testing (linting)

These modules need to be available for testing and, in MDAL's case, for
operation. 

This should pull the submodules down:

    git submodule init
    git submodule sync
    git submodule update --init --recursive

Though there is almost certainly a more compact syntax available to carry this
out.

### Creating a configuration file

A configuration file is required for the application and a sample is provided
in the root directory.  For initial work however the defaults should suffice,
so creating an empty file will work, or copy the sample and make sure
everything is commented out.  The configuration file for the running app is
expected in the `instance/` sub-directory.

    $ mkdir instance
    $ touch instance/myapp.conf

### Retrieving static resources

### Start up the application

The following command line tells Flask which app to run, what environment to
use, establishes the paths for necessary modules, and specifies a port to use
other than the default:

    $ FLASK_APP=src FLASK_ENV=development PYTHONPATH=.:mdal flask run -p 4001
     * Serving Flask app 'src' (lazy loading)
     * Environment: development
     * Debug mode: on
    20220415.140849 DEBUG   Initializing logger for myapp
    20220415.140849 INFO    Application initialized: 0+local.dev.version
    20220415.140849 INFO     * Running on http://127.0.0.1:4001 (Press CTRL+C to quit)
    20220415.140849 INFO     * Restarting with stat
     * Debugger is active!
     * Debugger PIN: 821-470-903
    20220415.140849 DEBUG   Initializing logger for myapp
    20220415.140849 INFO    Application initialized: 0+local.dev.version
