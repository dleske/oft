# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
from .exceptions import AppException, BadCall

# ---------------------------------------------------------------------------
#                                                                   HELPERS
# ---------------------------------------------------------------------------


# ---------------------------------------------------------------------------
#                                                               exceptions
# ---------------------------------------------------------------------------

class NotAuthorized(AppException):
  """
  Exception raised when a user authorization call fails.
  """

# ---------------------------------------------------------------------------
#                                                               AUTHZ CLASS
# ---------------------------------------------------------------------------

class Authz:

  def __init__(self, config=None):
    """ Initialize Authorization class.

    Args:
      config: dict describing configuration.  Contents of this dict are
        defined by the specialization (subclass) of this class.
    """
    self._config = config

    # dictionaries for all details about authorization
    self._details = {}
    self._entitlements = []

  @property
  def details(self):
    """ Authorized user details.

    Authorization may provide information about the user in addition to any
    provided during authentication.
    """
    return self._details or None

  @property
  def entitlements(self):
    """ Authorization entitlements.

    Entitlements are authorization labels associated with an identity.  They
    can indicate roles or privileges or other abstracts.
    """
    return self._entitlements or None

  def authorize(self, user, data=None):
    """ Authorize user.

    Authorize user and populate whatever details and entitlements are
    available as a result of that operation.

    Returns:
      Boolean indicating whether user is authorized for any role.
    """
    raise NotImplementedError

# ---------------------------------------------------------------------------
#                                                        authorizer helpers
# ---------------------------------------------------------------------------

authorizer_class = None

def register_authorizer(authorizer):

  # pylint: disable=global-statement
  global authorizer_class

  if authorizer_class is not None:
    raise BadCall("Can only register one authorizer")

  authorizer_class = authorizer

def get_authorizer():
  return authorizer_class()
