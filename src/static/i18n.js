"use strict";

/**
 * Translate named message with variable interpolation
 *
 * Messages can be defined with numbered variable stubs ("$1", "$2", etc.)
 * which are replaced with current values on translation.
 *
 * @param {String} message - Name of message defined in i18n/<lang>.js
 * @param {...*} args - Variables to interpolate in translated message
 *
 * @returns {String} Translated message
 */
function i18n() {
  let message = arguments[0];
  let translation = i18n_strings[message];
  if (!translation) {
    return(`"${message}" NOT DEFINED`);
  }

  let args = arguments;
  function replacer(match, offset, string) {
    return args[offset];
  }
  if (arguments.length > 1) {
    return translation.replaceAll(/\$(\d+)/g, replacer);
  }

  return translation;
}

// convenience alias for familiarity
var _ = i18n;

/**
 * Find all static translatable text elements in document and replace them
 * with their appropriate translations.
 */
function i18n_static() {
  // find all translatable elements
  let elements = document.querySelectorAll("[data-i18n]");

  elements.forEach((element) => {
    let name = element.dataset.i18n;
    let text = i18n_strings[name];
    if (text) {
      element.innerHTML = text;
    } else {
      element.innerHTML = `"${name}" NOT DEFINED`;
    }
  });
}


/**
 * Determine preferred language from user agent.
 *
 * @param supported {Array} List of language codes supported by the
 *   application, ordered by preference.
 * @returns {String} The two-character major language code.
 */
function preferred_language(supportedLanguages) {

  // if primary language defined by browser is supported, use that
  if (navigator.language.slice(0,2) in supportedLanguages) {
    return(navigator.language.slice(0,2));
  }

  // navigator.languages is experimental, though widely supported
  // https://developer.mozilla.org/en-US/docs/Web/API/Navigator/languages
  for (let i = 0; i < navigator.languages.length; i++) {
    if (supportedLanguages.includes(navigator.languages[i].slice(0,2))) {
      return(navigator.languages[i].slice(0,2));
    }
  }

  // best we can do is to assume English: in 2001, 86% of Canadians understand
  // English and 32% understand French
  // https://en.wikipedia.org/wiki/Languages_of_Canada, "Bilingualism and
  // multilingualism vs English-French bilingualism"
  return('en');
}

function epoch_to_local_time(epoch) {
  // epochs in Javascript are milliseconds but we're using seconds
  let d = new Date(epoch * 1000);
  return(d.toLocaleString());
}

function timestamp_to_local_time(timestamp) {
  let d = new Date(timestamp);
  return(d.toLocaleString());
}
