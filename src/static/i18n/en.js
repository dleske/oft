i18n_strings = {
    // basic reusable stuff
    "ID": "ID",
    "CANCEL": "Cancel",
    "SUBMIT": "Submit",
    "REVERT": "Revert",
    "TITLE": "Title",
    "BODY": "Body",
    "OKAY": "Okay",
    "NAME": "Name",
    "CONFIRM_DELETE": "Confirm deletion",

    // status strings
    "LOADING": "Loading...",

    // info and error alert titles
    "INFO": "Info",
    "ALERT": "Alert",
    "ERROR": "Error",

    // application info
    "ABOUT": "About",
    "ABOUT_CONTENT": `<p>My Application is an application for people who like
applications and want to apply themselves in the application of computer
applications to problems with known applications.</p>

<p>Oh yeah I know you're glad you asked.</p>`,

    // error messages
    "NO_3RD_PARTY": "Could not initialize preferences and other resources." +
      " This indicates some resources are not available to be loaded by your" +
      " browser, and the application will not function or display correctly." +
      " We apologize for the inconvenience.",

    "VERSION": "Version: $1",
}
