"use strict";

/**
* A wrapper around Bootstrap 5's Tabs implementation to ease programmatic
* use with basic tabs management.
*/
class Tabs {

  /**
  * @param {string} idBase Prefix used for tab IDs
  * @param {string} tabDivId DOM ID of the DIV node to be a container for the
  *        tabbed layout.
  * @param {function} activeFunction The callback function to execute in
  *        response to tab selection events.
  */
  constructor(idBase, tabDivId, activeFunction) {
    this._idBase = idBase;
    this._activeFunction = activeFunction;
    this._active = null;

    let div = document.getElementById(tabDivId);
    if (div == null) {
      // problem
      // TODO
    }

    this._tabs = document.createElement('ul');
    this._tabs.className = 'nav nav-tabs';
    this._tabs.setAttribute('id', `${idBase}-tabs`);
    this._tabs.setAttribute('role', 'tablist');
    div.appendChild(this._tabs);

    this._panesId = `${idBase}-panes`;
    this._panes = document.createElement('div');
    this._panes.className = 'tab-content';
    this._panes.setAttribute('id', `${idBase}-panes`);
    div.appendChild(this._panes);
  }

  /**
  * Add a new tab and provide its content pane.
  *
  * The new tab is added at the end of the existing tabs unless a tag's ID is
  * specified for `insertBeforeId`.
  *
  * @param {string} id Unique identifier for the tab.
  * @param {string} name Descriptive display name for the tab.
  * @param {string=} insertBeforeId Insert new tab before the tab with
  *     this ID.
  */
  add(id, name, insertBeforeId=null) {
    const tabId = `${this._idBase}-${id}-tab`;
    const paneId = `${this._idBase}-${id}-pane`;

    // create tab
    const tab = document.createElement('li');
    tab.className = 'nav-item';
    tab.setAttribute('id', `${tabId}-li`);
    tab.setAttribute('role', 'presentation');

    // inside tab, create button
    const tabButton = document.createElement('button');
    tabButton.className = 'nav-link';
    tabButton.setAttribute('id', tabId);
    tabButton.setAttribute('role', 'tab');
    tabButton.setAttribute('data-id', id);
    tabButton.setAttribute('data-bs-toggle', 'tab');
    tabButton.setAttribute('data-bs-target', `#${paneId}`);
    tabButton.setAttribute('aria-selected', 'false');
    tabButton.textContent = name;
    tab.appendChild(tabButton);

    // create bootstrap tab entity for new tab
    const tabBootstrap = new bootstrap.Tab(tabButton);

    // add event listener
    //let activeFunction = this._activeFunction;
    let tabs = this;
    tab.addEventListener('click', function(event) {
      event.preventDefault();
      tabBootstrap.show();
      tabs._activeFunction(event.target.dataset.id);
      tabs._active = event.target.dataset.id;
    });

    // create tab pane
    const pane = document.createElement('div');
    pane.className = 'tab-pane fade';
    pane.setAttribute('id', paneId);
    pane.setAttribute('role', 'tabpanel');
    pane.setAttribute('aria-labelled-by', tabId);

    // add tab and pane to containers
    if (insertBeforeId != null) {
      // determine tab element for given ID
      const insertBefore = document.getElementById(`${this._idBase}-${insertBeforeId}-tab-li`);

      if (insertBeforeId == null) {
        // TODO: error
      }

      // insert before that tab
      this._tabs.insertBefore(tab, insertBefore);
    }
    else {
      this._tabs.appendChild(tab);
    }
    this._panes.appendChild(pane);

    return pane;
  }

  /**
  * Make the tab with the given ID the active tab.  The previously active tab
  * is explicitly hidden and removed from associated classes.
  *
  * @param {string} id The tab to make active
  */
  activate(id) {
    const tabId = `${this._idBase}-${id}-tab`;
    const paneId = `${this._idBase}-${id}-pane`;

    if (this._active != null) {
      let inactiveTab = document.getElementById(`${this._idBase}-${this._active}-tab`);
      inactiveTab.classList.remove('active');
      inactiveTab.setAttribute('aria-selected', 'false');
      let inactivePane = document.getElementById(`${this._idBase}-${this._active}-pane`);
      inactivePane.classList.remove('show');
      inactivePane.classList.remove('active');
    }

    let activeTab = document.getElementById(tabId);
    activeTab.classList.add('active');
    activeTab.setAttribute('aria-selected', 'true');
    let activePane = document.getElementById(paneId);
    activePane.classList.add('show');
    activePane.classList.add('active');

    this._active = id;
  }

  /**
  * Updates the tab title and returns the content pane for update
  *
  * Something about this or that.
  *
  * @param {string} id The tab ID
  * @param {string} text Updated text for the tab, or null if don't change
  * @return {DOM node} The tab's content pane
  */
  update(id, text=null) {

    if (text != null) {
      const tabId = `${this._idBase}-${id}-tab`;
      document.getElementById(tabId).textContent = text;
    }

    const paneId = `${this._idBase}-${id}-pane`;
    return document.getElementById(paneId);
  }
}
