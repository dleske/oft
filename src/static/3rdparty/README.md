# Third party static resources

It may be desirable to isolate third-party resources from the main Flask
application, for example to leave it out of version control, to differentiate
it from the application's code base, to handle updating independently, or for
other reasons.

For deployment, third-party resources may be deployed to a static webserver
and their location described by the `static_resource_uri` configuration
variable.

Contents of this directory, apart from this file, are ignored by Git.

## External resources used in the application template

* [jQuery](https://jquery.com/)
* [Bootstrap](https://getbootstrap.com/)
* [js-cookie](https://github.com/js-cookie/js-cookie)

The following has been used in some applications and may be useful:

* [DataTables](https://www.datatables.net/)
