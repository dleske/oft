/**
 * User preferences stored in cookies.
 * 
 * Requires Cookies class (https://github.com/js-cookie/js-cookie)
 */
class Preferences {
  /**
   * Initialize, retrieving previously stored preferences if available.
   *
   * @param {String} cookie_name - name under which preferences are stored.
   */
  constructor(cookie_name) {
    this._cookie = cookie_name;
    let prefsJSON = Cookies.get(cookie_name, { 'SameSite': 'strict' });
    if (prefsJSON) {
      this._prefs = JSON.parse(prefsJSON);
    } 
    else {
      this._prefs = {};
    }
  }

  /**
   * Write preferences to user storage.
   */
  write() {
    let prefsJSON = JSON.stringify(this._prefs);
    Cookies.set(this._cookie, prefsJSON, { 'SameSite': 'strict' });
  }

  get(key, def) {
    const property = Object.getOwnPropertyDescriptor(this._prefs, key);
    if (property == null) {
      return def;
    }
    return property.value || def;
  }

  set(key, value) {
    Object.defineProperty(this._prefs, key, {value: value, writable: true, enumerable: true});
    this.write();
  }
}

/* Example usage of Preferences class ---------------------
class MyPreferences extends Preferences {
  getActiveTab() {
    return this._prefs['activeTab'] || 'firstTabId';
  }

  setActiveTab(tab_id) {
    this._prefs['activeTab'] = tab_id;
    this.write();
  }
}

const myprefs = new MyPreferences("myprefscookie");
------------------------------------------------------------- */
