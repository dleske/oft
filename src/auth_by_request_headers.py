# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:

from flask import current_app
from .auth import Auth, register_authenticator
from .exceptions import BadConfig

class AuthByRequestHeaders(Auth):

  def authenticate(self, data):
    """ Authenticate user via HTTP request headers.

    Authenticate user based on request headers and populate whatever details
    are available.

    Args:
      data: HTTP request headers

    Returns:
      Boolean indicating whether user is authenticated.
    """
    if current_app.env != 'production' and current_app.config.get('AUTHX_USER_OVERRIDE'):
      self._user = current_app.config['AUTHX_USER_OVERRIDE']
      return True

    header = current_app.config.get('AUTHX_HTTP_HEADER_USER')
    if header is None:
      raise BadConfig('Missing definition for HTTP request header containing user ID')
    self._user = data.get(header, None)

    # TODO: replace with more other available information if possible
    if self._user:
      self._details['display_name'] = self._user

    return self._user is not None

register_authenticator(AuthByRequestHeaders)
