# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
from flask import Blueprint, render_template, redirect, session, url_for
from .auth import login_required

bp = Blueprint('dashboard', __name__)

# ---------------------------------------------------------------------------
#                                                          DATABASE HELPERS
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
#                                                                   HELPERS
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
#                                                                     ROUTES
# ---------------------------------------------------------------------------

# This is the user dashboard
@bp.route('/')
def index():

  # redirect admin to admin dashboard if that's the view they want
  if session.get('admin_view'):
    return redirect(url_for('admin.admin'))

  return render_template('user/index.html')

# this is for getting at the user dashboard if you're an admin
@bp.route('/user')
def user_view_redirect():

  if 'admin' in session['entitlements']:
    session['admin_view'] = False
  return redirect(url_for('dashboard.index'))

# this is for authenticating if that's desired
@bp.route('/login')
@login_required
def login():

  # redirect admin to admin dashboard if that's the view they want
  if session.get('admin_view'):
    return redirect(url_for('admin.admin'))

  return redirect(url_for('dashboard.index'))
