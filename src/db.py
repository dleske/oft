# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import os
import click
from flask import current_app
from flask.cli import with_appcontext
import medial
from .log import get_log
from .exceptions import DatabaseException, UnsupportedDatabase

# Current database schema version
# format is string: YYYYMMDD
#
# This number must match the latest entry in the database's schemalog table,
# or an upgrade should be performed.
#
SCHEMA_VERSION = '20211005'

# query to fetch latest schema version
SQL_GET_SCHEMA_VERSION = """
  SELECT    version
  FROM      schemalog
  ORDER BY  version DESC
  LIMIT     1
"""

# TODO: define this here?
SQL_SCRIPTS_DIR = 'sql'


def init_db(schema=None):
  db = medial.get_db()

  if not schema:
    if db.type == 'sqlite':
      schema = f"{SQL_SCRIPTS_DIR}/schema.sql"
    elif db.type == 'postgres':
      schema = f"{SQL_SCRIPTS_DIR}/schema.psql"
    else:
      exception_source = f"{type(db).__module}.{type(db).__qualname__}"
      raise UnsupportedDatabase(
        f"Did not catch proper DB connection type.  Source: {exception_source}"
      )

  get_log().info("Initializing database with %s", schema)
  with current_app.open_resource(schema) as f:
    db.executescript(f.read().decode('utf8'))
  db.commit()


def seed_db(seedfile):
  db = medial.get_db()

  get_log().info("Seeding database with %s", seedfile)
  with current_app.open_resource(seedfile) as f:
    db.executescript(f.read().decode('utf8'))
  db.commit()


def get_schema_version():
  db = medial.get_db()
  try:
    vers = db.execute(SQL_GET_SCHEMA_VERSION).fetchone()['version']
  except Exception as e:
    get_log().error("Error in retrieving schema version: %s", e)
    raise DatabaseException("Could not retrieve schema version") from e
  if not vers:
    get_log().error("Could not find latest schema version")
    raise DatabaseException("Could not determine schema version")
  return (vers, SCHEMA_VERSION)


@click.command('init-db')
@with_appcontext
def init_db_command():
  """Clear the existing data and create new tables."""
  init_db()
  click.echo('Initialized the database.')


@click.command('seed-db')
@click.argument('seedfile')
@with_appcontext
def seed_db_command(seedfile):
  """
  Clear existing data, create new tables, seed with test data.

  Note:
    Seedfile here is interpreted to be relative to client's current working
    directory.
  """

  init_db()
  seed_db(os.path.join(os.getcwd(), seedfile))
  click.echo('Initialized and seeded the database.')
