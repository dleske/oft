# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=W0621
#
from flask import Blueprint
from .db import get_schema_version

# establish blueprint
bp = Blueprint('status', __name__, url_prefix='/status')

# ---------------------------------------------------------------------------
#                                                                    HELPERS
# ---------------------------------------------------------------------------

def _check_status_app(statuses):

  # this is trivial
  statuses.append("I'm: Okay")
  return 200

def _check_status_db(statuses):

  status = 200

  # get schema version from database
  try:
    (actual, expected) = get_schema_version()
  except Exception as e:
    statuses.append(f"DB: Caught exception: {str(e).rstrip()}")
    status = 500
  else:
    if actual == expected:
      statuses.append(f"DB: Okay (schema version {actual})")
    else:
      statuses.append(f"DB: Schema version mismatch: {actual}, expected {expected}")
      status = 500

  return status

# ---------------------------------------------------------------------------
#                                                                     ROUTES
# ---------------------------------------------------------------------------

@bp.route('/', methods=['GET'])
def get_status():
  """
  Reports app health apart from external dependencies.  This is so limited
  in order that it can be used as a liveness probe in Kubernetes.  Failing
  this would then result in restarts of the container, and basing that on
  the connections to LDAP, OTRS, etc. does not make sense.
  """

  statuses = []
  status = 200

  # run some tests
  status = _check_status_app(statuses)

  status_all = "\n".join(statuses)
  return status_all, status, {'Content-type': 'text/plain; charset=utf-8'}

@bp.route('/services/db', methods=['GET'])
def get_services_status_db():

  statuses = []
  status = 200

  status = max(status, _check_status_db(statuses))

  status_all = "\n".join(statuses)
  return status_all, status, {'Content-type': 'text/plain; charset=utf-8'}

@bp.route('/services', methods=['GET'])
def get_services_status():

  statuses = []
  status = 200

  status = max(status, _check_status_db(statuses))

  status_all = "\n".join(statuses)
  return status_all, status, {'Content-type': 'text/plain; charset=utf-8'}
