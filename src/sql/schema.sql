DROP TABLE IF EXISTS schemalog;

CREATE TABLE schemalog (
  version VARCHAR(10) PRIMARY KEY,
  applied TIMESTAMP
);
INSERT INTO schemalog (version, applied) VALUES ('20211005', CURRENT_TIMESTAMP);
