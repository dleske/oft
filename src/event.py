# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
from .config import codename as application_tag
from .notifier import get_notifiers
from .log import get_log
from .exceptions import ImpossibleException

# ---------------------------------------------------------------------------
#                                                               SQL queries
# ---------------------------------------------------------------------------

# TODO: will need query for "get notifiers associated with this type of event"

# ---------------------------------------------------------------------------
#                                                        base Event classes
# ---------------------------------------------------------------------------

class Event():

  def __init__(self, id=None, timestamp=None):
    self._id = id
    self._timestamp = timestamp

  def __str__(self):
    return f"{self.__class__.__name__}: {self._message}"

  # pylint: disable=no-self-use
  def notifiable(self):
    return False

  @property
  def id(self):
    if self._id is None:
      raise ImpossibleException('There is no ID defined on this object; was it just created?')
    return self._id

  @property
  def timestamp(self):
    return self._timestamp

  def serialize(self):
    d = {
      key.lstrip('_'): val
      for (key, val) in self.__dict__.items()
    }
    d['type'] = self.__class__.__name__
    return d

class ReportReceived(Event):

  def __init__(self, message):
    self._message = message
    super().__init__()

  def notifiable(self):
    return True

# ---------------------------------------------------------------------------
#                                                                   helpers
# ---------------------------------------------------------------------------

# this is where events are stored
events = {}

def register_event(type, cls):
  events[type] = cls

def report(event):

  get_log().debug("In report(%s)", event)

  if not event.notifiable():
    return

  notifiers = get_notifiers()
  if notifiers:
    for notifier in notifiers:
      notifier.notify(f"{application_tag}: {event}")
