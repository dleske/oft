# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=raise-missing-from
#
from flask import Blueprint
#from flask import Blueprint, jsonify, request, g, session
#from werkzeug.exceptions import BadRequest
#from .auth import login_required, admin_required
#from .log import get_log
#from .errors import xhr_error, xhr_success
#from .exceptions import ResourceNotFound, BadCall, AppException, ResourceNotCreated

bp = Blueprint('ajax', __name__, url_prefix='/xhr')

# ---------------------------------------------------------------------------
#                                                          DATABASE HELPERS
# ---------------------------------------------------------------------------

# ---------------------------------------------------------------------------
#                                                                   HELPERS
# ---------------------------------------------------------------------------

# Given dict and a list of keys, returns list of any keys not occurring in
# dict.
def _must_have(dct, keys):
  missing = [
    key
    for key in keys if key not in dct
  ]
  return missing or None

# ---------------------------------------------------------------------------
#                                                         ROUTES - ...
# ---------------------------------------------------------------------------
