# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import sys
import os
import logging
import configparser
from json import JSONEncoder
from flask import Flask, session, current_app
from flask_babel import _
import mergeconf
import medial

# app definitions
from . import config

# utilities
from . import log
from . import db

# blueprints
from . import admin
from . import auth
from . import authz
from . import dashboard
from . import ajax
from . import status

# implementations
from . import auth_by_request_headers
from . import authz_by_groups

# misc
from . import version
from . import errors
from . import i18n

# ---------------------------------------------------------------------------
#                                                                 helpers
# ---------------------------------------------------------------------------

# override default JSON encoder to look for `serializable` method
def json_encoder_override(self, obj):
  # pylint: disable=unused-argument
  return getattr(
    obj.__class__,
    "to_dict",
    json_encoder_override.default
  )(obj)
json_encoder_override.default = JSONEncoder().default
JSONEncoder.default = json_encoder_override

# load variables into templating environment
def inject_custom_vars():
  return dict(
    app=config.codename,
    title=current_app.config['APPLICATION_TITLE'],
    css_override=current_app.config['CSS_OVERRIDE'],
    resources_uri=current_app.config['STATIC_RESOURCE_URI'],
    documentation_uri=current_app.config['DOCUMENTATION_URI'],
    version=version.version,
    language=i18n.get_locale()
  )

# ---------------------------------------------------------------------------
#                                                               application
# ---------------------------------------------------------------------------

def create_app(test_config=None):

  # initialize logging
  log.init_log(config.codename)

  # create Flask app object
  app = Flask('src', instance_relative_config=True)

  # initialize Babel
  i18n.babel.init_app(app)

  # load configuration
  conf = config.determine_config(app.instance_path, test_config)

  # load the instance config
  app.config.from_mapping(conf)
  app.config.from_mapping({'SECRET_KEY': 'dev'})

  init_app(app)

  app.register_blueprint(auth.bp)

  app.register_blueprint(dashboard.bp)
  app.add_url_rule('/', endpoint='index')

  app.register_blueprint(admin.bp)
  app.register_blueprint(ajax.bp)
  app.register_blueprint(status.bp)

  # this is to make get_locale() available to templates
  app.jinja_env.globals.update(get_locale=i18n.get_locale)

  # register error handlers
  app.register_error_handler(400, errors.error_400)
  app.register_error_handler(403, errors.error_403)
  app.register_error_handler(404, errors.error_404)
  app.register_error_handler(500, errors.error_500)

  # register generic error handler, unless testing
  if (not app.config.get('TESTING', False)
      and app.config.get('CATCH_ALL_EXCEPTIONS', True)):
    app.register_error_handler(Exception, errors.generic_exception)

  # register custom context processor to add custom variables
  app.context_processor(inject_custom_vars)

  # initialize database
  medial.configure(app.config['DATABASE_URI'])

  # log startup and version
  log.get_log().info("Application initialized: %s", version.version)

  return app


def init_app(app):
  app.teardown_appcontext(medial.close)
  app.teardown_appcontext(log.close_log)
  app.cli.add_command(db.init_db_command)
  app.cli.add_command(db.seed_db_command)
