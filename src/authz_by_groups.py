# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import re
from flask import current_app
from .authz import Authz, register_authorizer

class AuthzByGroups(Authz):

  # pattern for splitting comma-separated string into array
  _re = re.compile(r',\s*')

  def authorize(self, user, data=None):
    """ Authorize user.

    Authorize user and populate whatever details and entitlements are
    available as a result of that operation.

    Returns:
      Boolean indicating whether user is authorized for any role.
    """
    authorized = False

    # check if user is in list of authorized users
    if current_app.config.get('AUTHZ_USERS'):
      users = self.__class__._re.split(current_app.config['AUTHZ_USERS'])
      if user in users:
        self._entitlements.append('user')
        authorized = True

    # check if user is in list of admins
    if current_app.config.get('AUTHZ_ADMINS'):
      admins = self.__class__._re.split(current_app.config['AUTHZ_ADMINS'])
      if user in admins:
        self._entitlements.append('admin')
        authorized = True

    return authorized

register_authorizer(AuthzByGroups)
