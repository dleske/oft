# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=global-statement
#
import logging

_logger = None


def init_log(tag, level=logging.DEBUG):
  """ Initialize logging mechanism

  Initialize logging for the application with the given tag and severity.
  """
  global _logger

  if not _logger:
    # initialize logging
    # this should be on a formatter attached to a handler but simple for now
    logging.basicConfig(format='%(asctime)s %(levelname)-7s %(message)s',
                        datefmt='%Y%m%d.%H%M%S')
    _logger = logging.getLogger(tag)
    _logger.setLevel(level)

    _logger.debug("Initializing logger for %s", tag)


def get_log():
  """ Retrieve logger
  """
  if not _logger:
    init_log(__package__)

  return _logger


def close_log(e=None):
  """ Close logger
  """
  if _logger:
    if e:
      clean_e = str(e).strip().replace('\n', ' :: ')
      _logger.info("Closing logger in presence of error condition: %s", clean_e)
    else:
      _logger.debug("Closing down logger")

  # actually nothing to close
