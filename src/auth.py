# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint: disable=assigning-non-slot
# NOTE: "assigning-non-slot" test is broken in Pylint; can remove when
#       https://github.com/PyCQA/pylint/issues/3793 resolved
#
import functools

from flask import (
  Blueprint, g, request, session
)
from werkzeug.exceptions import abort

from .log import get_log
from .authz import NotAuthorized, get_authorizer
from .exceptions import BadCall

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.before_app_request
def load_logged_in_user():

  user_id = session.get('user')

  if user_id is None:
    g.user = None
  else:
    g.user = session['user']


def authn_authz():

  if g.user is None:

    # clear any existing login cruft
    session.clear()

    # get authenticator
    authenticator = get_authenticator()

    # retrieve authenticated-user header from request, if present
    if not authenticator.authenticate(request.headers):
      get_log().error("User not authenticated")
      return 401

    # determine authorization
    authorizer = get_authorizer()
    try:
      authorizer.authorize(authenticator.user)
    except NotAuthorized as e:
      get_log().warning("Unauthorized user: %s", e)
      return 403

    # get user information into session
    session['user'] = authenticator.user
    session.update(authenticator.details or {})
    session.update(authorizer.details or {})

    # check entitlements
    if not authorizer.entitlements:
      get_log().warning("Unauthorized user: %s", authenticator.user)
      return 403
    session['entitlements'] = authorizer.entitlements

    load_logged_in_user()

  return 200


def login_required(view):
  @functools.wraps(view)
  def wrapped_view(**kwargs):

    rc = authn_authz()
    if rc != 200:
      abort(rc)

    if 'entitlements' not in session or 'user' not in session['entitlements']:
      get_log().info("Forbidding unentitled user: %s", session['user'])
      abort(403)

    return view(**kwargs)

  return wrapped_view


def admin_required(view):
  @functools.wraps(view)
  def wrapped_view(**kwargs):

    rc = authn_authz()
    if rc != 200:
      abort(rc)

    if 'entitlements' not in session or 'admin' not in session['entitlements']:
      get_log().info("Forbidding unentitled admin: %s", session['user'])
      abort(403)

    return view(**kwargs)
  return wrapped_view


def authz_required(entitlement):
  def decorator(view):
    @functools.wraps(view)
    def wrapped_view(*args, **kwargs):

      rc = authn_authz()
      if rc != 200:
        abort(rc)

      if 'entitlements' not in session or entitlement not in session['entitlements']:
        get_log().info("Forbidding user %s not entitled for '%s'", session['user'], entitlement)
        abort(403)

      return view(*args, **kwargs)
    return wrapped_view
  return decorator


# ---------------------------------------------------------------------------
#                                                                AUTH CLASS
# ---------------------------------------------------------------------------

class Auth:

  def __init__(self, config=None):
    """ Initialize Authenticator class.

    Args:
      config: dict describing configuration.  Contents of this dict are
        defined by the specialization (subclass) of this class.
    """
    self._config = config
    self._user = None

    # dictionaries for details about user once authenticated
    self._details = {}

  @property
  def user(self):
    """ Return user identifier.
    """
    return self._user

  @property
  def details(self):
    """ Authenticated user details.

    Authorization may provide information about the user in addition to any
    provided during authentication.
    """
    return self._details or None

  def authenticate(self, data):
    """ Authenticate user.

    Authenticate user based on given data and populate whatever details are
    available as a result of that operation.

    Returns:
      Boolean indicating whether user is authenticated.
    """
    raise NotImplementedError

# ---------------------------------------------------------------------------
#                                                     authenticator helpers
# ---------------------------------------------------------------------------

authenticator_class = None

def register_authenticator(authenticator):

  # pylint: disable=global-statement
  global authenticator_class

  if authenticator_class is not None:
    raise BadCall("Can only register one authenticator")

  authenticator_class = authenticator

def get_authenticator():
  return authenticator_class()
