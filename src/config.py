# vi: set softtabstop=2 ts=2 sw=2 expandtab:
# pylint:
#
import os
import sys
import mergeconf
from . import log

# ---------------------------------------------------------------------------
#                                                     application variables
# ---------------------------------------------------------------------------

# application codename: short name used for tags, prefixes, etc.
name = 'Dummy Application'
codename = 'dummy'

# ---------------------------------------------------------------------------
#                                                                   methods
# ---------------------------------------------------------------------------

def flatten_merged_config(config):
  """ Flatten mergeconf configuration into key-value dictionary for Flask.

  Hierarchical mergeconf configuration is flattened into a key-value lookup
  where each key represents the sections hierarchy followed by the item key,
  separated with underscores, and uppercased, so ['database']['uri'] becomes
  becomes 'DATABASE_URI', so that the configuration can be understood by
  Flask.
  """

  def flatten_dict(d, prefix=''):
    myd = {}
    for key, value in d.items():
      if isinstance(value, dict):
        myd.update(flatten_dict(value, prefix=f'{prefix}{key.upper()}_'))
      else:
        myd[f'{prefix}{key.upper()}'] = value
    return myd

  return flatten_dict(config.to_dict())


def determine_config(path, test_config=None):
  """ Determine application configuration from available sources

  Given instance path and optionally a test configuration, and assuming a
  configuration file at either the default location or overridden in the
  environment, as well as other variables set in the environment, build a
  merged application configuration from various sources and return as a flat
  key-value dictionary.

  Args:
    path (str): base path to look for files in their default locations, such
      as SQLite database file or configuration file.
    test_config (map): key-value pairs of configuration set for testing

  Returns:
    map: key-value pairs to be used as in Flask app configuration.
  """

  # determine default database URI based on instance location
  default_database_uri = f"file:///{path}/{codename}.sqlite"

  # determine default config file
  default_config_file = f"{path}/{codename}.conf"

  # determine configuraiton file to use
  config_file = default_config_file
  if test_config:
    config_file = test_config.get('CONFIG', config_file)
  config_file = os.environ.get(f'{codename}_CONFIG', config_file)

  # initialize configuration
  conf = mergeconf.MergeConf(codename, strict=True, files=config_file)
  conf.add('application_title', name, mandatory=True, description='Full display name of application')
  conf.add('css_override', description='CSS to override style elements for this instance')
  conf.add('documentation_uri', description='URI to link for application documentation')
  conf.add('static_resource_uri', '/static/3rdparty', description='Base URI for static resources')
  conf.add('catch_all_exceptions', False, description='Catch all exceptions so user gets no stack trace')
  db_conf = conf.add_section('database')
  db_conf.add('uri', default_database_uri, mandatory=True,
    description='URI for database with format scheme://user:password@host/dbname')
  authx_conf = conf.add_section('authx')
  authx_conf.add('http_header_user', 'X-Forwarded-User',
    description='Request header containing authenticated user name')
  authx_conf.add('user_override', description='(TESTING ONLY) set user ID manually')
  authz_conf = conf.add_section('authz')
  authz_conf.add('users', description='comma-separated list of user identifiers for regular users')
  authz_conf.add('admins', description='comma-separated list of user identifiers for users with admin role')

  # TODO: merge in test config if specified
  # ...

  # merge configuration from various potential sources
  try:
    conf.merge()
  except mergeconf.exceptions.UndefinedSection as e:
    log.get_log().fatal("Unrecognized configuration section: %s", e.section)
    sys.exit(1)
  except mergeconf.exceptions.UndefinedConfiguration as e:
    log.get_log().fatal("Unrecognized configuration item: %s (section %s)", e.item, e.section)
    sys.exit(1)
  except mergeconf.exceptions.MissingConfigurationFile as e:
    log.get_log().fatal("Missing configuration file: %s", e.file)
    sys.exit(1)
  except mergeconf.exceptions.MissingConfiguration as e:
    log.get_log().fatal("Missing configuration items: %s", e.missing)
    sys.exit(1)

  return flatten_merged_config(conf)
